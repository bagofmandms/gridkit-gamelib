#include "Lib/canvas.hpp"
int main(){
    sf::ContextSettings settings;
    settings.antialiasingLevel = 4;
    sf::RenderWindow window(sf::VideoMode(1000, 500), "Test pane", sf::Style::Default, settings);
    CANVAS::Canvas gameWindow(&window);
    return 0;
}