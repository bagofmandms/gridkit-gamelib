#ifndef CANVAS
#define CANVAS
#include <SFML/Graphics.hpp>
#include <unistd.h>
#include <iostream>
class Canvas{
    public:
        Canvas(sf::RenderWindow* window);
        void AddElement();
};
#endif