#ifndef ELEMENT
#define ELEMENT
class Element{
    public: 
        bool draw;
        virtual ~Element() {}
        virtual void DrawElement() = 0;
};
#endif