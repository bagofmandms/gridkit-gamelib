#ifndef GRID
#define GRID
#include "element.hpp"
#include <SFML/Graphics.hpp>
class Grid: public Element{
    public:
        Grid(): Element(){};
        void DrawElement() override;
};
#endif